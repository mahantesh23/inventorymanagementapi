from flask import Flask
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='postgresql:///InventoryAPI'

db=SQLAlchemy(app)



class Inventory(db.Model):

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(250), nullable=False)
	description =db. Column(db.String(250))
	price = db.Column(db.Integer, nullable=False)


class Customer(db.Model):

	name = db.Column(db.String(80), nullable=False)
	id = db.Column(db.Integer, primary_key=True)


def __init__(self,name,description,price):
	self.name=name
	self.description=description
	self.price=price


if __name__=='__main__':
	db.create_all()
	app.run(debug=True)