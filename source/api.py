from flask import Flask
from flask_restplus import Resource, Api
from source import app

api = Api(app, version='1.0', title='Sample API', description='A sample API')


@api.route('/hello')
class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}
